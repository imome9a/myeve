#!/bin/bash
# remember these things
echo "Here's your reminder... "
echo ""
echo "Working with QEMU disks:"
echo "qemu-img create -f qcow2 virtioa.qcow2 30G"
echo "qemu-img resize <virtioa.qcow2 path> +10G"
echo "qemu-img convert -f vdi -O qcow2 ubuntu.vdi ubuntu.qcow2 "
