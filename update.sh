#!/bin/bash

# update
echo "Updating EVE-NG"
apt update

# upgrade
echo "Upgrading EVE-NG"
apt dist-upgrade

# message
echo "You need to reboot EVE-NG for updates to take affect"


