#!/bin/bash

# vars
keyname=storage

# generate ssh keys
ssh-keygen -f ~/.ssh/$keyname -q -P ""

# copy to storage
ssh-copy-id -i ~/.ssh/$keyname.pub root@storage.home.net
