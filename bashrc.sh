#!/bin/bash -i

# rinse and rebuilt bashrc
rm ~/.bashrc
cp /etc/skel/.bashrc ~

# append my alias to bashrc
echo "updating bashrc"
echo "" >> ~/.bashrc
echo "# my alias" >> ~/.bashrc
echo "alias cdqemu='cd /opt/unetlab/addons/qemu'" >> ~/.bashrc
echo "alias cdscripts='cd ~/scripts/eveng'" >> ~/.bashrc
echo "alias cdtemps='cd /opt/unetlab/html/templates/intel'" >> ~/.bashrc
echo "alias fixperms='~/scripts/eveng/fixperms.sh'" >> ~/.bashrc
echo "alias nodecommit='~/scripts/eveng/nodecommit.sh'" >> ~/.bashrc
echo "alias remember='~/scripts/eveng/remember.sh'" >> ~/.bashrc
echo "alias sync='~/scripts/eveng/sync.sh'" >> ~/.bashrc
echo "alias update='~/scripts/eveng/update.sh'" >> ~/.bashrc
echo "alias stopall='/opt/unetlab/wrappers/unl_wrapper -a stopall'" >> ~/.bashrc

# confirm bashrc changes
echo
echo "view changes..."
cat ~/.bashrc

# update source
source ~/.bashrc
