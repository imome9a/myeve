#!/bin/bash

# vars
storage=storage.home.net
keyfile="~/.ssh/storage"

echo "syncing qemu..."
echo
#sshpass -p $pass rsync -r -v -e ssh --ignore-existing --progress root@$storage:/mnt/user/Lab/qemu /opt/unetlab/addons/
rsync -r -v -e "ssh -i $keyfile -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --ignore-existing --progress root@$storage:/mnt/user/Lab/qemu /opt/unetlab/addons/

echo "syncing templates..."
echo
#sshpass -p $pass rsync -r -v -e ssh --progress root@$storage:/mnt/user/Lab/extras/templates/ /opt/unetlab/html/templates/intel/
rsync -r -v -e "ssh -i $keyfile -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress root@$storage:/mnt/user/Lab/extras/templates/ /opt/unetlab/html/templates/intel/

echo "starting icons..."
echo
#sshpass -p $pass rsync -r -v -e ssh --progress root@$storage:/mnt/user/Lab/extras/icons/ /opt/unetlab/html/images/icons/
#sshpass -p $pass rsync -r -v -e ssh --progress root@$storage:/mnt/user/Lab/extras/net-icons/ /opt/unetlab/html/images/net_icons/
rsync -r -v -e "ssh -i $keyfile -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress root@$storage:/mnt/user/Lab/extras/icons/ /opt/unetlab/html/images/icons/
rsync -r -v -e "ssh -i $keyfile -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress root@$storage:/mnt/user/Lab/extras/net-icons/ /opt/unetlab/html/images/net_icons/

echo "fixing perms..."
echo
/opt/unetlab/wrappers/unl_wrapper -a fixpermissions
