#!/bin/bash
# commit an eve-ng node to master node
# first parameter must be lab id
# second parameter must be node id

echo "Make sure your node is stopped before running this command"
echo ""

# if statement evaluating number of parameters was provided
if [ $# = 2 ]; then
  echo "The lab id is $1 and the node id is $2"
  /opt/qemu/bin/qemu-img commit /opt/unetlab/tmp/0/$1/$2/*.qcow2
else
  echo "You need to provide both the lab id and the node id im order to successfully commit your node changes."
fi
